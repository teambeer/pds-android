package org.udg.pds.pds_android.activity;

//import org.udg.pds.pds_android.fragment.PDSResponderFragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ListFragment;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;
import org.udg.pds.pds_android.R;
import org.udg.pds.pds_android.fragment.OldUsers;
import org.udg.pds.pds_android.fragment.RESTResponderUsuari;
import org.udg.pds.pds_android.adapter.UsuarisAdapter;
import org.udg.pds.pds_android.fragment.Users;
import org.udg.pds.pds_android.xml.XMLError;


public class UsuarisActivity extends FragmentActivity
 implements Users.OnUserListListener {

    //private UsuarisAdapter mAdapter;
    private Users responder;
    //ListFragment list;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rest_service);

        FragmentManager     fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();

        // RESTResponderFragments call setRetainedInstance(true) in their onCreate() method. So that means
        // we need to check if our FragmentManager is already storing an instance of the responder.
        responder = (Users) fm.findFragmentByTag("Users");
        if (responder == null) {
            responder = new Users();
            responder.setArguments(this.getIntent().getExtras());

            // We add the fragment using a Tag since it has no views. It will make the Twitter REST call
            // for us each time this Activity is created.
            ft.add(R.id.fragment_content, responder);
        }

        // Make sure you commit the FragmentTransaction or your fragments
        // won't get added to your FragmentManager. Forgetting to call ft.commit()
        // is a really common mistake when starting out with Fragments.
        ft.commit();
    }


    @Override
    public void updateUserList() {
        responder.getUsuaris();
    }

    @Override
    public void onErrorUsuaris(XMLError e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}