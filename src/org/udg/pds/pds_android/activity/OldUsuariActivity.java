package org.udg.pds.pds_android.activity;

//import org.udg.pds.pds_android.fragment.PDSResponderFragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ListFragment;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import org.udg.pds.pds_android.R;
import org.udg.pds.pds_android.adapter.UsuarisAdapter;
import org.udg.pds.pds_android.fragment.Users;


public class OldUsuariActivity extends FragmentActivity{

    private UsuarisAdapter mAdapter;
    ListFragment list;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rest_service);

        mAdapter = new UsuarisAdapter(this, R.layout.usuari_layout);

        FragmentManager     fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();

        // Since we are using the Android Compatibility library
        // we have to use FragmentActivity. So, we use ListFragment
        // to get the same functionality as ListActivity.
        list = new ListFragment();
        ft.add(R.id.fragment_content, list);

        // Let's set our list adapter to a simple ArrayAdapter.
        list.setListAdapter(mAdapter);




        //Obtenir informacio entre activitats
        // Estamos en OtraActivity
        //String rec_user_alias = activity.getIntent().getStringExtra("user_alias");
        //String rec_user_pass = activity.getIntent().getStringExtra("user_pass");

        // O también de esta otra forma

        // Estamos en OtraActivity
        /*Bundle datos = this.getIntent().getExtras();
        int recuperamos_variable_integer = datos.getInt("variable_integer");
        String recuperamos_variable_string = datos.getString("variable_string");
        float recuperamos_variable_float = datos.getFloat("objeto_float");
        */

        // RESTResponderFragments call setRetainedInstance(true) in their onCreate() method. So that means
        // we need to check if our FragmentManager is already storing an instance of the responder.
        Users responder = (Users) fm.findFragmentByTag("Users");
        if (responder == null) {
            responder = new Users();
            responder.setArguments(this.getIntent().getExtras());

            // We add the fragment using a Tag since it has no views. It will make the Twitter REST call
            // for us each time this Activity is created.
            ft.add(responder, "Users");
        }

        // Make sure you commit the FragmentTransaction or your fragments
        // won't get added to your FragmentManager. Forgetting to call ft.commit()
        // is a really common mistake when starting out with Fragments.
        ft.commit();
    }

    public UsuarisAdapter getUsuarisAdapter() {
        return mAdapter;
    }
}