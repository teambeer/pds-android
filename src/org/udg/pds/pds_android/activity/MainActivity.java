package org.udg.pds.pds_android.activity;

//import org.udg.pds.pds_android.fragment.RESTResponderFragments;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.widget.Button;
import android.view.View;
import org.udg.pds.pds_android.R;
import org.udg.pds.pds_android.data.BeerListREST;
import org.udg.pds.pds_android.fragment.BeerList;
import org.udg.pds.pds_android.fragment.Login;
import org.udg.pds.pds_android.fragment.MainMenu;
import org.udg.pds.pds_android.fragment.RESTResponder;
import org.udg.pds.pds_android.util.Dialog;
import org.udg.pds.pds_android.xml.XMLError;
import org.udg.pds.pds_android.xml.XMLTaskList;
import org.udg.pds.pds_android.xml.XMLUsuari;


public class MainActivity extends FragmentActivity
        implements Login.OnLoginListener,
        RESTResponder.onRESTResponderListener,
        BeerList.OnBeerListListener{

    // This is the REST responder that this fragment will use to perform the REST calls
    // Remember that all the calls to the REST Service have to be made from a Fragment
    // because the Activity can be destroyed with a configuration change in the middle of a call
    private RESTResponder mResponder;
    private BeerList mBeer;
    private MainMenu mMain;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        FragmentManager     fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();

        // First we check if the responder has been already created. If not, initialize
        mResponder = (RESTResponder) fm.findFragmentByTag("RESTResponder");
        if (mResponder == null) {
            mResponder = new RESTResponder();
            // We add the fragment using a Tag since it has no views.
            ft.add(mResponder, "RESTResponder");
            // Make sure you commit the FragmentTransaction or your fragments
            // won't get added to your FragmentManager. Forgetting to call ft.commit()
            // is a really common mistake when starting out with Fragments.
            ft.commit();
        }

        // Check if the TaskList fragment exists. If not it means that the user has not authenticated
        // show the login fragment
        /*BeerList ul = (BeerList) fm.findFragmentByTag("BeerList");
        if (ul == null) {
            login();
            return;
        }

        // If the fragment exists, add it to the activity
        ft = fm.beginTransaction();
        ft.add(R.id.fragment_content, ul, "BeerList");
        ft.commit();
        */

        mMain = (MainMenu) fm.findFragmentByTag("MainMenu");
        if (mMain == null) {
            login();
            return;
        }

        //ft.replace(R.id.fragment_content, mMain, "MainMenu");
        //ft.commit();


    }

    /**
     * Shows the login fragment
     */
    void login() {
        FragmentManager     fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        Login l = new Login();
        ft.add(R.id.fragment_content, l);
        ft.commit();
    }

    // This method is called when the "Login" button is pressed in the Login fragment
    @Override
    public void checkCredentials(String username, String password) {
        mResponder.doAuthentication(username, password);
    }

    // This method is called from RESTResponder if the authenticantion has been successful
    @Override
    public void onAuthSuccessful(XMLUsuari u) {
        // Once the user has authenticated we can remove the login fragment and add the TaskList one
        FragmentManager     fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        //mBeer = new BeerList();
        //ft.replace(R.id.fragment_content, mBeer, "BeerList");
        mMain = new MainMenu();
        ft.replace(R.id.fragment_content, mMain, "MainMenu");
        ft.commit();
        //this.updateBeerList();
    }

    // This function is called whenever we want to update the task list
    // It just calls the responder to make the REST call
    /*@Override
    public void onI() {
        mResponder.getTasks();
    } */

    // This method is called from RESTResponder if any server operation had an error
    @Override
    public void onError(XMLError e) {
        Dialog.onError("Error", this, e, new ResultReceiver(new Handler()) {
            @Override
            protected void onReceiveResult(int resultCode, Bundle resultData) {
            }
        });
    }

    // This function is called from RESTResponder whenever the task list needs to be updated
    // This function is just the communication point between the RESTResponder fragment and the
    // TaskList fragment. Remember that the way that Fragments communicate it is through
    // the activity to which they are attached
    @Override
    public void onBeerListUpdate(BeerListREST bl) {
        //To change body of implemented methods use File | Settings | File Templates.
        mBeer.showBeerList(bl);
    }

    @Override
    public void updateBeerList() {
        //To change body of implemented methods use File | Settings | File Templates.
        mResponder.getBeers();
    }

    @Override
    public void onErrorCerveses(XMLError e) {
        //To change body of implemented methods use File | Settings | File Templates.
        Dialog.onError("Error", this, e, new ResultReceiver(new Handler()) {
            @Override
            protected void onReceiveResult(int resultCode, Bundle resultData) {
            }
        });
    }
}