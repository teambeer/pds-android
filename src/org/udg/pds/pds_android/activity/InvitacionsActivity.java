package org.udg.pds.pds_android.activity;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ListFragment;
import org.udg.pds.pds_android.R;
import org.udg.pds.pds_android.adapter.BeersAdapter;
import org.udg.pds.pds_android.adapter.UsuarisAdapter;
import org.udg.pds.pds_android.data.BeerListREST;
import org.udg.pds.pds_android.data.UsuariListREST;
import org.udg.pds.pds_android.fragment.*;
import org.udg.pds.pds_android.util.Dialog;
import org.udg.pds.pds_android.xml.XMLError;
import org.udg.pds.pds_android.xml.XMLUsuari;

/**
 * Created with IntelliJ IDEA.
 * User: Norbert
 * Date: 08/05/13
 * Time: 02:18
 * To change this template use File | Settings | File Templates.
 */
public class InvitacionsActivity extends FragmentActivity
            implements InvitacionsList.OnInvitacionsListListener
{
    //private UsuarisAdapter uAdapter;
    private InvitacionsList responder;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rest_service);

        FragmentManager     fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();

        // RESTResponderFragments call setRetainedInstance(true) in their onCreate() method. So that means
        // we need to check if our FragmentManager is already storing an instance of the responder.
         responder = (InvitacionsList) fm.findFragmentByTag("InvitacionsList");
        if (responder == null) {
            responder = new InvitacionsList();

            responder.setArguments(getIntent().getExtras());
            // We add the fragment using a Tag since it has no views. It will make the Twitter REST call
            // for us each time this Activity is created.
            ft.add(R.id.fragment_content, responder);
        }

        // Make sure you commit the FragmentTransaction or your fragments
        // won't get added to your FragmentManager. Forgetting to call ft.commit()
        // is a really common mistake when starting out with Fragments.
        ft.commit();
    }

    @Override
    public void updateUserList() {
        responder.getInvitations();
    }

    @Override
    public void onError(XMLError e) {
        //To change body of implemented methods use File | Settings | File Templates.
        Dialog.onError("Error", this, e, new ResultReceiver(new Handler()) {
            @Override
            protected void onReceiveResult(int resultCode, Bundle resultData) {
            }
        });
    }

    @Override
    public void onInvitacionsListUpdate(UsuariListREST ul) {
        //To change body of implemented methods use File | Settings | File Templates.
        responder.showUsuariList(ul);
    }


}
