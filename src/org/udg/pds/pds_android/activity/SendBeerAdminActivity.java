package org.udg.pds.pds_android.activity;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.*;
import org.udg.pds.pds_android.R;
import org.udg.pds.pds_android.data.BeerListREST;
import org.udg.pds.pds_android.fragment.*;
import org.udg.pds.pds_android.util.Dialog;
import org.udg.pds.pds_android.xml.XMLCorrecte;
import org.udg.pds.pds_android.xml.XMLError;
import org.udg.pds.pds_android.xml.XMLUsuari;

//import org.udg.pds.pds_android.fragment.PDSResponderFragmentPerfilUsuari;

/**
 * Created with IntelliJ IDEA.
 * User: Norbert
 * Date: 19/04/13
 * Time: 01:04
 * To change this template use File | Settings | File Templates.
 */
public class SendBeerAdminActivity extends FragmentActivity
    implements SendBeer.OnSendBeerListener{

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.create_beer);

            Button btnEnviar = (Button)this.findViewById(R.id.btnEnviar);
            btnEnviar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //To change body of implemented methods use File | Settings | File Templates.
                    SendBeerToAdmin();
                }
            });

            ArrayAdapter adapter;
            String array_spinner[];
            Spinner spf = (Spinner)this.findViewById(R.id.spinnerFermentacio);
            array_spinner=new String[2];
            array_spinner[0]="Alta fermentacio";
            array_spinner[1]="Baixa fermentacio";
            adapter = new ArrayAdapter(this,
                    android.R.layout.simple_spinner_item, array_spinner);
            spf.setAdapter(adapter);

            Spinner spa = (Spinner)this.findViewById(R.id.spinnerAspecte);
            array_spinner=new String[5];
            array_spinner[0]="Ambre";
            array_spinner[1]="Vermella";
            array_spinner[2]="Rossa";
            array_spinner[3]="Negra";
            array_spinner[4]="Terbola";

            adapter = new ArrayAdapter(this,
                    android.R.layout.simple_spinner_item, array_spinner);
            spa.setAdapter(adapter);

        }

        /**
         * Shows the login fragment
         */
    void SendBeerToAdmin() {
        FragmentManager     fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        SendBeer sb = new SendBeer();
        //Obtenir dades de la vista
        EditText etNom = (EditText)findViewById(R.id.etSendBeerNom);
        EditText etMarca = (EditText)findViewById(R.id.etSendBeerMarca);
        EditText etFotografia = (EditText)findViewById(R.id.etSendBeerFotografia);
        EditText etGraduacio = (EditText)findViewById(R.id.etSendBeerGraduacio);
        Spinner spf = (Spinner)this.findViewById(R.id.spinnerFermentacio);
        Spinner spa = (Spinner)this.findViewById(R.id.spinnerAspecte);

        try
        {
            Bundle bundle = new Bundle();
            bundle.putString("nom",etNom.getText().toString());
            bundle.putString("marca",etMarca.getText().toString());
            bundle.putString("fotografia",etFotografia.getText().toString());
            bundle.putInt("graduacio",Integer.parseInt(etGraduacio.getText().toString()));
            bundle.putString("fermentacio",spf.getSelectedItem().toString());
            bundle.putString("aspecte",spa.getSelectedItem().toString());

            //passar dades al fragment
            sb.setArguments(bundle);

            ft.add(sb,"SendBeer");
            ft.commit();
        }
        catch (Exception ex)
        {
            Toast.makeText(this,ex.getMessage(),Toast.LENGTH_SHORT);
        }

    }

    @Override
    public void OnErrorSendBeer(XMLError e) {
        //To change body of implemented methods use File | Settings | File Templates.
        Dialog.onError("Error", this, e, new ResultReceiver(new Handler()) {
            @Override
            protected void onReceiveResult(int resultCode, Bundle resultData) {
            }
        });
    }

    @Override
    public void OnSuccefulSendBeer(XMLCorrecte c) {
        //To change body of implemented methods use File | Settings | File Templates.
        Dialog.onCorrecte("Correcte", this, c, new ResultReceiver(new Handler()) {
            @Override
            protected void onReceiveResult(int resultCode, Bundle resultData) {
            }
        });
        this.finish();
    }
}
