package org.udg.pds.pds_android.activity;

import android.view.View;
import android.widget.EditText;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.content.Intent;
import org.udg.pds.pds_android.R;


public class PrincipalActivity extends FragmentActivity{


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_user);
    }

    public void onBtnClicked(View v)
    {
        if(v.getId() == R.id.pulsadorLogin)
        {
            validarUsuari();
        }
    }

    public void validarUsuari() {
        EditText etAlies = ((EditText) findViewById(R.id.login_nickname));
        EditText etPass = (EditText) findViewById(R.id.login_pass);
        String sAlies = etAlies.getText().toString();
        String sPass = etPass.getText().toString();
        // Pasaremos de la actividad actual a OtraActivity
        Intent intent = new Intent(this, UsuarisActivity.class);
        intent.putExtra("user_alias", sAlies);
        intent.putExtra("user_pass", sPass);
        startActivity(intent);

    }
}