package org.udg.pds.pds_android.activity;

//import org.udg.pds.pds_android.fragment.PDSResponderFragment;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import org.udg.pds.pds_android.R;
import org.udg.pds.pds_android.fragment.BeerList;
import org.udg.pds.pds_android.adapter.BeersAdapter;
import org.udg.pds.pds_android.util.Dialog;
import org.udg.pds.pds_android.xml.XMLError;

public class BeerActivity extends FragmentActivity
    implements BeerList.OnBeerListListener
    {
    
    //private BeersAdapter mAdapter;
    private BeerList mBeer;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rest_service);
        
        //mAdapter = new BeersAdapter(this, R.layout.beer_layout);
        
        FragmentManager     fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        
        // Since we are using the Android Compatibility library
        // we have to use FragmentActivity. So, we use ListFragment
        // to get the same functionality as ListActivity.
        //ListFragment list = new ListFragment();
        //ft.add(R.id.fragment_content, list);
        
        // Let's set our list adapter to a simple ArrayAdapter.
        //list.setListAdapter(mAdapter);
        
        // RESTResponderFragments call setRetainedInstance(true) in their onCreate() method. So that means
        // we need to check if our FragmentManager is already storing an instance of the responder.
        mBeer = (BeerList) fm.findFragmentByTag("BeerList");
        if (mBeer == null) {
            mBeer = new BeerList();
            
            // We add the fragment using a Tag since it has no views. It will make the Twitter REST call
            // for us each time this Activity is created.
            ft.add(R.id.fragment_content, mBeer);
        }

        // Make sure you commit the FragmentTransaction or your fragments
        // won't get added to your FragmentManager. Forgetting to call ft.commit()
        // is a really common mistake when starting out with Fragments.
        ft.commit();
    }

    /*public BeersAdapter getBeersAdapter() {
        return mAdapter;
    }*/

    @Override
    public void updateBeerList() {
        //To change body of implemented methods use File | Settings | File Templates.
        mBeer.getCerveses();
    }

    @Override
    public void onErrorCerveses(XMLError e) {
        //To change body of implemented methods use File | Settings | File Templates.
        Dialog.onError("Error", this, e, new ResultReceiver(new Handler()) {
            @Override
            protected void onReceiveResult(int resultCode, Bundle resultData) {
            }
        });
    }
}