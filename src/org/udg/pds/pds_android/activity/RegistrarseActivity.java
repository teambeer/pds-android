package org.udg.pds.pds_android.activity;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import org.udg.pds.pds_android.R;
import org.udg.pds.pds_android.fragment.RegistreUsuari;
import org.udg.pds.pds_android.util.Dialog;
import org.udg.pds.pds_android.xml.XMLError;

/**
 * Created with IntelliJ IDEA.
 * User: Norbert
 * Date: 24/04/13
 * Time: 19:58
 * To change this template use File | Settings | File Templates.
 */
public class RegistrarseActivity extends FragmentActivity
        implements RegistreUsuari.OnRegister{

    private RegistreUsuari mResponder;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_user);

        FragmentManager     fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();

        // First we check if the responder has been already created. If not, initialize
        mResponder = (RegistreUsuari) fm.findFragmentByTag("RegistreUsuari");
        if (mResponder == null) {
            mResponder = new RegistreUsuari();
            // We add the fragment using a Tag since it has no views.
            ft.add(mResponder, "RegistreUsuari");
            // Make sure you commit the FragmentTransaction or your fragments
            // won't get added to your FragmentManager. Forgetting to call ft.commit()
            // is a really common mistake when starting out with Fragments.
            ft.commit();
        }
    }

    @Override
    public void Registrarse(String alies, String pass, String passConfirmacio, String email) {
        //To change body of implemented methods use File | Settings | File Templates.
        mResponder.ServerRegister(alies, pass, passConfirmacio, email);
    }

    @Override
    public void onError(XMLError e) {
        //To change body of implemented methods use File | Settings | File Templates.
        Dialog.onError("Error", this, e, new ResultReceiver(new Handler()) {
            @Override
            protected void onReceiveResult(int resultCode, Bundle resultData) {
            }
        });
    }
}
