package org.udg.pds.pds_android.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import org.udg.pds.pds_android.R;
import org.udg.pds.pds_android.data.BeerListREST.BeerREST;
import org.udg.pds.pds_android.xml.XMLCommentList;


public class ComentariAdapter extends ArrayAdapter<XMLCommentList.XMLComment> {

    int resource;
    String response;
    Context context;

    private LayoutInflater inflater = null;

    //Initialize adapter
    public ComentariAdapter(Context context, int resource) {
        super(context, resource);
        this.resource=resource;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
     
     
    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        LinearLayout alertView;
        //Get the current alert object
        XMLCommentList.XMLComment m = getItem(position);
         
        View vi = convertView;
        //Inflate the view
        if(convertView==null)
        {
        	vi = inflater.inflate(R.layout.comment_layout, null);
        }
        //Get the text boxes from the listitem.xml file
        TextView tvComentari =(TextView)vi.findViewById(R.id.beerCommentText);
         
        //Assign the appropriate data from our alert object above
        tvComentari.setText(m.contingut);

        return vi;
    }
 
}