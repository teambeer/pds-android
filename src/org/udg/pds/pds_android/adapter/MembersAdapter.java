package org.udg.pds.pds_android.adapter;
 
import java.util.List;

import org.udg.pds.pds_android.R;
import org.udg.pds.pds_android.data.MemberListREST.MemberREST;
 
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
 
 
public class MembersAdapter extends ArrayAdapter<MemberREST> {
 
    int resource;
    String response;
    Context context;
    
    private LayoutInflater inflater = null;

    //Initialize adapter
    public MembersAdapter(Context context, int resource) {
        super(context, resource);
        this.resource=resource;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
     
     
    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        LinearLayout alertView;
        //Get the current alert object
        MemberREST m = getItem(position);
         
        View vi = convertView;
        //Inflate the view
        if(convertView==null)
        {
        	vi = inflater.inflate(R.layout.member_layout, null);
        }
        //Get the text boxes from the listitem.xml file
        TextView idText =(TextView)vi.findViewById(R.id.memberViewId);
        TextView idEmail =(TextView)vi.findViewById(R.id.memberViewEmail);
         
        //Assign the appropriate data from our alert object above
        idText.setText(String.valueOf(m.id));
        idEmail.setText(m.email);
         
        return vi;
    }
 
}