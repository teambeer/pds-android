package org.udg.pds.pds_android.xml;

import android.provider.ContactsContract;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import org.udg.pds.pds_android.data.BeerListREST;
import org.udg.pds.pds_android.data.UsuariListREST;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@XStreamAlias("collection")
public class XMLCommentList {
	
	@XStreamImplicit
	List<XMLComment> comments;

	public List<XMLComment> getCommentList() {
		return comments;
	}

    // IMPORTANT: declaring this method we allow xml strings like "<collection/>" to be
    // translated to a XMLTaksList with an empty list in the "tasks" field.
    // Otherwise we would get a null from the unmarshaling
    private Object readResolve() {
        if(comments == null){
            comments = new ArrayList<XMLComment>();
        }
        return this;
    }


    @XStreamAlias("comentari")
	public static class XMLComment {
        @XStreamAlias("id")
		public long id;
        @XStreamAlias("contingut")
        public String contingut;
        @XStreamAlias("cervesa")
        public BeerListREST.BeerREST cervesa;
        @XStreamAlias("usuari")
        public UsuariListREST.UsuariREST usuari;
        @XStreamAlias("data")
        public String data;

        @Override
		public String toString() {
			return id + " -> " + contingut;
		}
	}
}

