package org.udg.pds.pds_android.fragment;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.thoughtworks.xstream.XStream;
import org.udg.pds.pds_android.R;
import org.udg.pds.pds_android.activity.PerfilCervesaActivity;
import org.udg.pds.pds_android.activity.PerfilUsuariActivity;
import org.udg.pds.pds_android.data.BeerListREST;
import org.udg.pds.pds_android.data.UsuariListREST.UsuariREST;
import org.udg.pds.pds_android.service.RESTService;
import org.udg.pds.pds_android.xml.XMLError;
import org.udg.pds.pds_android.xml.XMLUsuari;

/**
 * Created with IntelliJ IDEA.
 * User: Norbert
 * Date: 19/04/13
 * Time: 01:05
 * To change this template use File | Settings | File Templates.
 */
public class PDSResponderFragmentPerfilCervesa extends Fragment {
    private static String TAG = PDSResponderFragmentPerfilCervesa.class.getName();

    private ResultReceiver mReceiver;
    private BeerListREST.BeerREST mBeer;

    // This is the public interface that the container activity has to implement
    // in order to use this fragment
    public interface onRESTPerfilCervesa{
        public void onDataReceivedSuccessful(BeerListREST.BeerREST beer);
        public void onError(XMLError e);
    }

    public PDSResponderFragmentPerfilCervesa() {
        mReceiver = new ResultReceiver(new Handler()) {

            @Override
            protected void onReceiveResult(int resultCode, Bundle resultData) {
                if (resultData != null && resultData.containsKey(RESTService.REST_RESULT)) {
                    onRESTResult(resultCode, resultData.getString(RESTService.REST_RESULT));
                }
                else {
                    onRESTResult(resultCode, null);
                }
            }

        };
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // This tells our Activity to keep the same instance of this
        // Fragment when the Activity is re-created during lifecycle
        // events. This is what we want because this Fragment should
        // be available to receive results from our RESTService no
        // matter what the Activity is doing.
        setRetainInstance(true);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // This gets called each time our Activity has finished creating itself.
        setBeerData();
    }

    private void setBeerData() {

        PerfilCervesaActivity activity = (PerfilCervesaActivity) getActivity();

        if (mBeer == null && activity != null) {
            // This is where we make our REST call to the service. We also pass in our ResultReceiver
            // defined in the RESTResponderFragment super class.

            // We will explicitly call our Service since we probably want to keep it as a private
            // component in our app. You could do this with Intent actions as well, but you have
            // to make sure you define your intent filters correctly in your manifest.
            Intent intent = new Intent(activity, RESTService.class);

            //Obtenir informacio pasada entre activitats
            // Estem en fragment
            int id_cervesa;
            Bundle bundle = activity.getIntent().getExtras();
            if(bundle.getInt("idCervesa")>0)
            {
                id_cervesa =  bundle.getInt("idCervesa");

                // IMPORTANT: canvieu l'URL depenent de si voleu treballar amb el servidor local o el OpenShift
                //intent.setData(Uri.parse("https://project2-pdsudg.rhcloud.com/rest/Usuaris"));
                intent.setData(Uri.parse("http://10.0.2.2:8080/rest/beers/"+id_cervesa));
                //intent.setData(Uri.parse("http://10.0.2.2:8080/rest/usuaris/"+id));


                // Here we are going to place our REST call parameters. Note that
                // we could have just used Uri.Builder and appendQueryParameter()
                // here, but I wanted to illustrate how to use the Bundle params.

                // Bundle params = new Bundle();

                intent.putExtra(RESTService.EXTRA_RESULT_RECEIVER, mReceiver);

                // Here we send our Intent to our RESTService.
                activity.startService(intent);
            }
        }
        else if (activity != null) {
            // Here we check to see if our activity is null or not.
            // We only want to update our views if our activity exists.

        }
    }

    public void onRESTResult(int code, String result) {
        // Here is where we handle our REST response. This is similar to the
        // LoaderCallbacks<D>.onLoadFinished() call from the previous tutorial.

        // Check to see if we got an HTTP 200 code and have some data.
        if (code == 200 && result != null) {

            onRESTPerfilCervesa mCallback = (onRESTPerfilCervesa)getActivity();

            try
            {
                mBeer = getCervesaFromXML(result);
                //assignar dades al perfil
                UpdatePerfil(mBeer);
                //callback
                mCallback = (onRESTPerfilCervesa)getActivity();
                mCallback.onDataReceivedSuccessful(mBeer);
            }
            catch (Exception ex) {
                // There has been an error
                XMLError e = getErrorFromXML(result);
                mCallback.onError(e);
            }
        }
        else {
            Activity activity = getActivity();
            if (activity != null) {
                Toast.makeText(activity, "Failed to load Usuari data. Check your internet settings.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private static BeerListREST.BeerREST getCervesaFromXML(String xml) {
        XStream xstream = new XStream(); // does not require XPP3 library starting with Java 6
        xstream.processAnnotations(BeerListREST.BeerREST.class);
        BeerListREST.BeerREST beerList = (BeerListREST.BeerREST) xstream.fromXML(xml);

        return beerList;
    }

    private static XMLError getErrorFromXML(String xml)
    {
        XStream xstream = new XStream(); // does not require XPP3 library starting with Java 6
        xstream.processAnnotations(XMLError.class);
        XMLError error = (XMLError) xstream.fromXML(xml);

        return error;
    }

    private void UpdatePerfil(BeerListREST.BeerREST b)
    {
        Activity a = getActivity();
        if (a!=null)
        {
            TextView tvPerfilNom = (TextView) a.findViewById(R.id.beerProfileNom);
            TextView tvPerfilMarca = (TextView) a.findViewById(R.id.beerProfileMarca);
            TextView tvPerfilGaduacio = (TextView) a.findViewById(R.id.beerProfileGraduacio);

            tvPerfilNom.setText(b.nom);
            tvPerfilMarca.setText(b.marca);
            tvPerfilGaduacio.setText(b.graduacio+"");

            Button btnComment = (Button) getActivity().findViewById(R.id.btnBeerComments);
            btnComment.setTag(Math.round(b.id));

            Button btnAddBeer = (Button) getActivity().findViewById(R.id.btnBeerAdd);
            btnAddBeer.setTag(Math.round(b.id));
        }
        else Toast.makeText(a, "Failed to load Usuari data. Check your internet settings.", Toast.LENGTH_SHORT).show();

    }
}
