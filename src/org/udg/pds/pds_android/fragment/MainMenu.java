package org.udg.pds.pds_android.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import org.udg.pds.pds_android.R;
import org.udg.pds.pds_android.activity.*;

/**
* Created with IntelliJ IDEA.
* User: imartin
* Date: 28/03/13
* Time: 15:49
* To change this template use File | Settings | File Templates.
*/


/**
* This Fragment contains the Tasks list layout
*/
public class MainMenu extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.switch_layout, container, false);

        /*Declaracio dels botons*/
        Button b;
        b = (Button) view.findViewById(R.id.btnMenuPerfil);
        // This is the listener to the "Add Task" button
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // When we press the "Add Task" button, the AddTask activity is called, where
                // we can introduce the data of the new task
                Intent i = new Intent(getActivity(), PerfilUsuariActivity.class);
                // We launch the activity with startActivityForResult because we want to know when
                // the launched activity has finished. In this case, when the AddTask activity has finished
                // we will update the list to show the new task.
                startActivity(i);
                //startActivityForResult(i, Global.RQ_ADD_TASK);
            }
        });
        /*Declaracio dels botons*/
        b = (Button) view.findViewById(R.id.btnMenuCerveses);
        // This is the listener to the "Add Task" button
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // When we press the "Add Task" button, the AddTask activity is called, where
                // we can introduce the data of the new task
                Intent i = new Intent(getActivity(), BeerActivity.class);
                // We launch the activity with startActivityForResult because we want to know when
                // the launched activity has finished. In this case, when the AddTask activity has finished
                // we will update the list to show the new task.
                startActivity(i);
                //startActivityForResult(i, Global.RQ_ADD_TASK);
            }
        });
        /*Declaracio dels botons*/
        b = (Button) view.findViewById(R.id.btnMenuCervesesUsuari);
        // This is the listener to the "Add Task" button
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // When we press the "Add Task" button, the AddTask activity is called, where
                // we can introduce the data of the new task
                Intent i = new Intent(getActivity(), UserBeersActivity.class);
                // We launch the activity with startActivityForResult because we want to know when
                // the launched activity has finished. In this case, when the AddTask activity has finished
                // we will update the list to show the new task.
                startActivity(i);
                //startActivityForResult(i, Global.RQ_ADD_TASK);
            }
        });
        /*Declaracio dels botons*/
        b = (Button) view.findViewById(R.id.btnMenuUsuaris);
        // This is the listener to the "Add Task" button
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // When we press the "Add Task" button, the AddTask activity is called, where
                // we can introduce the data of the new task
                Intent i = new Intent(getActivity(), UsuarisActivity.class);
                // We launch the activity with startActivityForResult because we want to know when
                // the launched activity has finished. In this case, when the AddTask activity has finished
                // we will update the list to show the new task.
                startActivity(i);
                //startActivityForResult(i, Global.RQ_ADD_TASK);
            }
        });
        /*Declaracio dels botons*/
        b = (Button) view.findViewById(R.id.btnMenuAmistats);
        // This is the listener to the "Add Task" button
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // When we press the "Add Task" button, the AddTask activity is called, where
                // we can introduce the data of the new task
                Intent i = new Intent(getActivity(), AmistatsActivity.class);
                // We launch the activity with startActivityForResult because we want to know when
                // the launched activity has finished. In this case, when the AddTask activity has finished
                // we will update the list to show the new task.
                startActivity(i);
                //startActivityForResult(i, Global.RQ_ADD_TASK);
            }
        });
        /*Declaracio dels botons*/
        b = (Button) view.findViewById(R.id.btnMenuInvitacions);
        // This is the listener to the "Add Task" button
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // When we press the "Add Task" button, the AddTask activity is called, where
                // we can introduce the data of the new task
                Intent i = new Intent(getActivity(), InvitacionsActivity.class);
                // We launch the activity with startActivityForResult because we want to know when
                // the launched activity has finished. In this case, when the AddTask activity has finished
                // we will update the list to show the new task.
                startActivity(i);
                //startActivityForResult(i, Global.RQ_ADD_TASK);
            }
        });
        /*Declaracio dels botons*/
        b = (Button) view.findViewById(R.id.btnMenuRegistrarUsuari);
        // This is the listener to the "Add Task" button
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // When we press the "Add Task" button, the AddTask activity is called, where
                // we can introduce the data of the new task
                Intent i = new Intent(getActivity(), RegistrarseActivity.class);
                // We launch the activity with startActivityForResult because we want to know when
                // the launched activity has finished. In this case, when the AddTask activity has finished
                // we will update the list to show the new task.
                startActivity(i);
                //startActivityForResult(i, Global.RQ_ADD_TASK);
            }
        });
        /*Declaracio dels botons*/
        b = (Button) view.findViewById(R.id.btnMenuAdministrar);
        // This is the listener to the "Add Task" button
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // When we press the "Add Task" button, the AddTask activity is called, where
                // we can introduce the data of the new task
                Intent i = new Intent(getActivity(), SendBeerAdminActivity.class);
                // We launch the activity with startActivityForResult because we want to know when
                // the launched activity has finished. In this case, when the AddTask activity has finished
                // we will update the list to show the new task.
                startActivity(i);
                //startActivityForResult(i, Global.RQ_ADD_TASK);
            }
        });
        /*Declaracio dels botons*/
        b = (Button) view.findViewById(R.id.btnMenuPerfilCervesa);
        // This is the listener to the "Add Task" button
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // When we press the "Add Task" button, the AddTask activity is called, where
                // we can introduce the data of the new task
                Intent i = new Intent(getActivity(), PerfilCervesaActivity.class);
                // We launch the activity with startActivityForResult because we want to know when
                // the launched activity has finished. In this case, when the AddTask activity has finished
                // we will update the list to show the new task.
                startActivity(i);
                //startActivityForResult(i, Global.RQ_ADD_TASK);
            }
        });
        return view;
    }
}