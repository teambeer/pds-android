package org.udg.pds.pds_android.fragment;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.v4.app.Fragment;
import com.thoughtworks.xstream.XStream;
import org.udg.pds.pds_android.activity.InvitacionsActivity;
import org.udg.pds.pds_android.adapter.UsuarisAdapter;
import org.udg.pds.pds_android.data.UsuariListREST;
import org.udg.pds.pds_android.service.RESTService;
import org.udg.pds.pds_android.util.Global;
import org.udg.pds.pds_android.xml.XMLError;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: Norbert
 * Date: 08/05/13
 * Time: 02:19
 * To change this template use File | Settings | File Templates.
 */
public class OldResponderInvitacions extends Fragment {

    UsuarisAdapter mAdapter;

    // We will use this to call the container activity on response from REST calls
    private onResponderInvitacions mCallback;

    // This is the public interface that the container activity has to implement
    // in order to use this fragment
    public interface onResponderInvitacions {
        public void onError(XMLError e);
        public void onInvitacionsListUpdate(UsuariListREST ul);
    }

    private static String TAG = OldResponderInvitacions.class.getName();

    // This is the XStream objet that we will use to comvert XML responses into objects
    private XStream mXstream = new XStream();

    private UsuariListREST mUsuaris; // It contains the data of the current tasks that we are showing

    public OldResponderInvitacions() {

        // Initalize XStream object to parse our XML responses from the server
        mXstream.processAnnotations(XMLError.class);
        mXstream.processAnnotations(UsuariListREST.class);
        mXstream.processAnnotations(UsuariListREST.UsuariREST.class);
    }

    @Override
    public void onAttach(Activity a) {
        super.onAttach(a);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallback = (onResponderInvitacions) a;
        } catch (ClassCastException e) {
            throw new ClassCastException(a.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // This tells our Activity to keep the same instance of this
        // Fragment when the Activity is re-created during lifecycle
        // events. This is what we want because this Fragment should
        // be available to receive results from our RESTService no
        // matter what the Activity is doing.
        setRetainInstance(true);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getInvitacions();
    }

    // This is the function used to retrieve all the tasks from an user from the server
    public void getInvitacions() {
        Activity activity =  getActivity();

        if (mUsuaris == null && activity != null) {
            // This is where we make our REST call to the service. We also pass in our ResultReceiver
            // defined in the RESTResponderFragment super class.

            // We will explicitly call our Service since we probably want to keep it as a private
            // component in our app. You could do this with Intent actions as well, but you have
            // to make sure you define your intent filters correctly in your manifest.
            Intent intent = new Intent(activity, RESTService.class);

            // Set the resource URL
            intent.setData(Uri.parse(Global.BASE_URL + "/rest/users/invitations"));
            // Set the HTTP verb
            intent.putExtra(RESTService.EXTRA_HTTP_VERB, RESTService.GET);

            // Finally we put a ResultReceiver into the intent, that the RESTService
            // will use to send back the results
            intent.putExtra(RESTService.EXTRA_RESULT_RECEIVER, new ResultReceiver(new Handler()) {
                @Override
                protected void onReceiveResult(int resultCode, Bundle resultData) {
                    if (resultCode == 200) {
                        String xml = resultData.getString(RESTService.REST_RESULT);
                        try {
                            UsuariListREST ul = (UsuariListREST) mXstream.fromXML(xml);
                            mCallback.onInvitacionsListUpdate(ul);
                        } catch (Exception ex) {
                            // There has been an error
                            XMLError e = (XMLError) mXstream.fromXML(xml);
                            mCallback.onError(e);
                        }
                    }
                }});

            // Here we send our Intent to our RESTService.
            activity.startService(intent);
        }
    }

    // This function is called from the container activity whenever it has received
    // a new task list from a REST responder
    public void showUsuariList(UsuariListREST ulr) {
        // Load our list adapter with our Tasks. That would cause and automatic update
        // of the corresponding ListView

        InvitacionsActivity activity = (InvitacionsActivity)getActivity();
        if (activity!=null)
        {
            //mAdapter=activity.getUsuarisAdapter();
            mAdapter.clear();
            List<UsuariListREST.UsuariREST> lu = ulr.getUsuarisList();
            if(lu != null){
                for (UsuariListREST.UsuariREST u : lu) {
                    mAdapter.add(u);
                }
            }
        }

    }
}
