package org.udg.pds.pds_android.fragment;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.thoughtworks.xstream.XStream;
import org.udg.pds.pds_android.R;
import org.udg.pds.pds_android.activity.EditPerfilUsuariActivity;
import org.udg.pds.pds_android.activity.PerfilUsuariActivity;
import org.udg.pds.pds_android.data.UsuariListREST.UsuariREST;
import org.udg.pds.pds_android.service.RESTService;
import org.udg.pds.pds_android.xml.XMLCorrecte;
import org.udg.pds.pds_android.xml.XMLError;

/**
 * Created with IntelliJ IDEA.
 * User: Norbert
 * Date: 19/04/13
 * Time: 01:05
 * To change this template use File | Settings | File Templates.
 */
public class EditPerfilUsuari extends Fragment {
    private static String TAG = EditPerfilUsuari.class.getName();

    private ResultReceiver mReceiver;
    private UsuariREST mUsuaris;
    private XStream mXstream = new XStream();


   /* public EditPerfilUsuari() {
        mReceiver = new ResultReceiver(new Handler()) {

            @Override
            protected void onReceiveResult(int resultCode, Bundle resultData) {
                if (resultData != null && resultData.containsKey(RESTService.REST_RESULT)) {
                    onRESTResult(resultCode, resultData.getString(RESTService.REST_RESULT));
                }
                else {
                    onRESTResult(resultCode, null);
                }
            }

        };
    }     */

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // This tells our Activity to keep the same instance of this
        // Fragment when the Activity is re-created during lifecycle
        // events. This is what we want because this Fragment should
        // be available to receive results from our RESTService no
        // matter what the Activity is doing.
        setRetainInstance(true);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        CarregarDades();

        Button b = (Button) getActivity().findViewById(R.id.btnDesar);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                desarPerfil();
            }
        });
    }

    private void CarregarDades()
    {
        Activity act = getActivity();

        ((EditText)act.findViewById(R.id.etPerfilEmail)).setText(getArguments().getString("email"));
        ((EditText)act.findViewById(R.id.etPerfilNom)).setText(getArguments().getString("alies"));
        ((EditText)act.findViewById(R.id.etPerfilPais)).setText(getArguments().getString("pais"));
        ((EditText)act.findViewById(R.id.etPerfilEdat)).setText(getArguments().getString("edat"));

    }

    private void desarPerfil() {
        //PerfilUsuariActivityCopia activity = (PerfilUsuariActivityCopia) getActivity();
        EditPerfilUsuariActivity activity = (EditPerfilUsuariActivity) getActivity();

        if (mUsuaris == null && activity != null) {

            Intent intent = new Intent(activity, RESTService.class);
            intent.setData(Uri.parse("http://10.0.2.2:8080/rest/users/updateprofile"));
            intent.putExtra(RESTService.EXTRA_HTTP_VERB, RESTService.POST);

            Bundle params = new Bundle();
            params.putString("nom", ((EditText)activity.findViewById(R.id.etPerfilNom)).getText().toString());
            params.putString("pais", ((EditText)activity.findViewById(R.id.etPerfilPais)).getText().toString());
            params.putString("email", ((EditText)activity.findViewById(R.id.etPerfilEmail)).getText().toString());
            params.putString("edat", ((EditText)activity.findViewById(R.id.etPerfilEdat)).getText().toString());
            intent.putExtra(RESTService.EXTRA_PARAMS, params);

            intent.putExtra(RESTService.EXTRA_RESULT_RECEIVER, new ResultReceiver(new Handler()) {
                // This function will be called from the REST Service when it invokes the
                // send() command of the ResultReceiver
                @Override
                protected void onReceiveResult(int resultCode, Bundle resultData) {
                    if (resultCode == 200) {
                        String xml = resultData.getString(RESTService.REST_RESULT);
                        try {
                            XMLCorrecte e = (XMLCorrecte) mXstream.fromXML(xml);
                            if (getActivity()!=null)    {
                                Toast.makeText(getActivity(), "Perfil desat", Toast.LENGTH_LONG).show();
                                Intent intent = new Intent(getActivity(), PerfilUsuariActivity.class);
                                startActivity(intent);
                                //getActivity().finish();
                            }
                        } catch (Exception ex) {
                            // There has been an error
                            XMLError e = (XMLError) mXstream.fromXML(xml);
                            //mRegister.onError(e);
                        }
                    }
                }
            });

            activity.startService(intent);
        }
        else if (activity != null) {
            // Here we check to see if our activity is null or not.
            // We only want to update our views if our activity exists.

        }
    }

    /*public void onRESTResult(int code, String result) {
        // Here is where we handle our REST response. This is similar to the
        // LoaderCallbacks<D>.onLoadFinished() call from the previous tutorial.

        // Check to see if we got an HTTP 200 code and have some data.
        if (code == 200 && result != null) {

            mUsuaris = getUsuariFromXML(result);
            UpdatePerfil(mUsuaris);
            //assignar dades al perfil
            /*
            Activity activity = getActivity();
            if (activity != null) {
                EditText etAlies = ((EditText) activity.findViewById(R.id.user_alias));
                etAlies.setText("usuari alies"); //TODO: corretgir, esta malament
            }     */


            //setTweets();
       /* }
        else {
            Activity activity = getActivity();
            if (activity != null) {
                Toast.makeText(activity, "Failed to load Usuari data. Check your internet settings.", Toast.LENGTH_SHORT).show();
            }
        }
    }    */

    private static UsuariREST getUsuariFromXML(String xml) {
        XStream xstream = new XStream(); // does not require XPP3 library starting with Java 6
        xstream.processAnnotations(UsuariREST.class);
        UsuariREST UsuariList = (UsuariREST) xstream.fromXML(xml);

        return UsuariList;
    }

    private void UpdatePerfil(UsuariREST u)
    {
        Activity a = getActivity();
        if (a!=null)
        {
            TextView tvPerfilEdat = (TextView) a.findViewById(R.id.tvPerfilEdat);
            TextView tvPerfilEmail = (TextView) a.findViewById(R.id.tvPerfilEmail);
            TextView tvPerfilNom = (TextView) a.findViewById(R.id.tvPerfilNom);
            TextView tvPerfilPais = (TextView) a.findViewById(R.id.tvPerfilPais);

            tvPerfilEdat.setText(u.id+"");
            tvPerfilNom.setText(u.alies);
            tvPerfilEmail.setText(u.pass);
            tvPerfilPais.setText("Andorra");


        }
        else Toast.makeText(a, "Failed to load Usuari data. Check your internet settings.", Toast.LENGTH_SHORT).show();

    }
}
