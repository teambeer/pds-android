package org.udg.pds.pds_android.fragment;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.thoughtworks.xstream.XStream;
import org.udg.pds.pds_android.R;
import org.udg.pds.pds_android.activity.EditPerfilUsuariActivity;
import org.udg.pds.pds_android.activity.PerfilUsuariActivity;
import org.udg.pds.pds_android.data.UsuariListREST;
import org.udg.pds.pds_android.service.RESTService;

/**
 * Created with IntelliJ IDEA.
 * User: Norbert
 * Date: 19/04/13
 * Time: 01:05
 * To change this template use File | Settings | File Templates.
 */
public class PDSResponderFragmentPerfilUsuari extends Fragment {
    private static String TAG = PDSResponderFragmentPerfilUsuari.class.getName();

    private ResultReceiver mReceiver;
    private UsuariListREST.UsuariREST mUsuaris;

    public PDSResponderFragmentPerfilUsuari() {
        mReceiver = new ResultReceiver(new Handler()) {

            @Override
            protected void onReceiveResult(int resultCode, Bundle resultData) {
                if (resultData != null && resultData.containsKey(RESTService.REST_RESULT)) {
                    onRESTResult(resultCode, resultData.getString(RESTService.REST_RESULT));
                }
                else {
                    onRESTResult(resultCode, null);
                }
            }

        };
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // This tells our Activity to keep the same instance of this
        // Fragment when the Activity is re-created during lifecycle
        // events. This is what we want because this Fragment should
        // be available to receive results from our RESTService no
        // matter what the Activity is doing.
        setRetainInstance(true);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // This gets called each time our Activity has finished creating itself.
        setTweets();

        Button btn = (Button) getActivity().findViewById(R.id.btnInvite);
        btn.setText("Editar");
        btn.setVisibility(View.VISIBLE);

        Button b = (Button) getActivity().findViewById(R.id.btnInvite);
        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                TextView pais = (TextView)getActivity().findViewById(R.id.tvPerfilPais);
                TextView edat = (TextView)getActivity().findViewById(R.id.tvPerfilEdat);

                Activity activity = getActivity();

                Intent intent = new Intent(activity, EditPerfilUsuariActivity.class);
                intent.putExtra("id", mUsuaris.id);
                intent.putExtra("alies", mUsuaris.alies);
                intent.putExtra("email", mUsuaris.email);
                intent.putExtra("pais", pais.getText().toString());
                intent.putExtra("edat", edat.getText().toString());
                startActivity(intent);
            }
        });
    }

    private void setTweets() {
        //PerfilUsuariActivityCopia activity = (PerfilUsuariActivityCopia) getActivity();
        PerfilUsuariActivity activity = (PerfilUsuariActivity) getActivity();

        if (mUsuaris == null && activity != null) {
            // This is where we make our REST call to the service. We also pass in our ResultReceiver
            // defined in the RESTResponderFragment super class.

            // We will explicitly call our Service since we probably want to keep it as a private
            // component in our app. You could do this with Intent actions as well, but you have
            // to make sure you define your intent filters correctly in your manifest.
            Intent intent = new Intent(activity, RESTService.class);

            //Obtenir informacio pasada entre activitats
            // Estem en fragment


            // IMPORTANT: canvieu l'URL depenent de si voleu treballar amb el servidor local o el OpenShift
            //intent.setData(Uri.parse("https://project2-pdsudg.rhcloud.com/rest/Usuaris"));
            intent.setData(Uri.parse("http://10.0.2.2:8080/rest/users/profile"));
            //intent.setData(Uri.parse("http://10.0.2.2:8080/rest/usuaris/"+id));


            // Here we are going to place our REST call parameters. Note that
            // we could have just used Uri.Builder and appendQueryParameter()
            // here, but I wanted to illustrate how to use the Bundle params.

            // Bundle params = new Bundle();

            intent.putExtra(RESTService.EXTRA_RESULT_RECEIVER, mReceiver);

            // Here we send our Intent to our RESTService.
            activity.startService(intent);
        }
        else if (activity != null) {
            // Here we check to see if our activity is null or not.
            // We only want to update our views if our activity exists.

        }
    }

    public void onRESTResult(int code, String result) {
        // Here is where we handle our REST response. This is similar to the
        // LoaderCallbacks<D>.onLoadFinished() call from the previous tutorial.

        // Check to see if we got an HTTP 200 code and have some data.
        if (code == 200 && result != null) {

            mUsuaris = getUsuariFromXML(result);
            UpdatePerfil(mUsuaris);
            //assignar dades al perfil
            /*
            Activity activity = getActivity();
            if (activity != null) {
                EditText etAlies = ((EditText) activity.findViewById(R.id.user_alias));
                etAlies.setText("usuari alies"); //TODO: corretgir, esta malament
            }           */


            //setTweets();
        }
        else {
            Activity activity = getActivity();
            if (activity != null) {
                Toast.makeText(activity, "Failed to load Usuari data. Check your internet settings.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private static UsuariListREST.UsuariREST getUsuariFromXML(String xml) {
        XStream xstream = new XStream(); // does not require XPP3 library starting with Java 6
        xstream.processAnnotations(UsuariListREST.UsuariREST.class);
        UsuariListREST.UsuariREST UsuariList = (UsuariListREST.UsuariREST) xstream.fromXML(xml);

        return UsuariList;
    }

    private void UpdatePerfil(UsuariListREST.UsuariREST u)
    {
        Activity a = getActivity();
        if (a!=null)
        {
            TextView tvPerfilEdat = (TextView) a.findViewById(R.id.tvPerfilEdat);
            TextView tvPerfilEmail = (TextView) a.findViewById(R.id.tvPerfilEmail);
            TextView tvPerfilNom = (TextView) a.findViewById(R.id.tvPerfilNom);
            TextView tvPerfilPais = (TextView) a.findViewById(R.id.tvPerfilPais);

            tvPerfilEdat.setText(u.id+"");
            tvPerfilNom.setText(u.alies);
            tvPerfilEmail.setText(u.pass);
            tvPerfilPais.setText("Andorra");


        }
        else Toast.makeText(a, "Failed to load Usuari data. Check your internet settings.", Toast.LENGTH_SHORT).show();

    }
}
