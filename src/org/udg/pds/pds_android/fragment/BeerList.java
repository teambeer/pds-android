package org.udg.pds.pds_android.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.thoughtworks.xstream.XStream;
import org.udg.pds.pds_android.R;
import org.udg.pds.pds_android.activity.BeerActivity;
import org.udg.pds.pds_android.activity.InvitacionsActivity;
import org.udg.pds.pds_android.activity.PerfilCervesaActivity;
import org.udg.pds.pds_android.activity.PerfilUsuariActivity;
import org.udg.pds.pds_android.adapter.BeersAdapter;
import org.udg.pds.pds_android.data.BeerListREST;
import org.udg.pds.pds_android.service.RESTService;
import org.udg.pds.pds_android.util.Global;
import org.udg.pds.pds_android.xml.XMLCommentList;
import org.udg.pds.pds_android.xml.XMLError;
import org.udg.pds.pds_android.xml.XMLTaskList;

import java.net.Proxy;
import java.util.List;

/**
* This Fragment contains the Tasks list layout
*/
public class BeerList extends ListFragment {

    // This is the interface that the container activity has to implement
    // in order to use this fragment
    public interface OnBeerListListener {
        public void updateBeerList();
        public void onErrorCerveses(XMLError e);
    }
    // We will use this to call the container activity on response from REST calls
    private OnBeerListListener mCallback;
    private BeersAdapter mAdapter;
    private BeerListREST mCerveses;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.beer_list, container, false);
        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // This tells our Activity to keep the same instance of this
        // Fragment when the Activity is re-created during lifecycle
        // events.
        setRetainInstance(true);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // Since onActivityCreated can be called multiple times in the Fragment lifecycle, we only initialize the
        // adapter if it is null (the first time)
        if (mAdapter == null) {
            mAdapter = new BeersAdapter(getActivity(), R.layout.beer_layout);
            //mAdapter = ((BeerActivity)getActivity()).getBeersAdapter();
            setListAdapter(mAdapter);
        }

        mCallback.updateBeerList();
    }

    @Override
    public void onAttach(Activity a) {
        super.onAttach(a);
        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            mCallback = (OnBeerListListener) a;
        } catch (ClassCastException e) {
            throw new ClassCastException(a.toString()
                    + " must implement OnBeerListListener");
        }
    }

    // This function is called from the container activity whenever it has received
    // a new task list from a REST responder
    public void showBeerList(BeerListREST bl) {
        // Load our list adapter with our Tasks. That would cause and automatic update
        // of the corresponding ListView
        mAdapter.clear();
        List<BeerListREST.BeerREST> bn = bl.getBeersList();
        if(bn != null){
            for (BeerListREST.BeerREST b : bn) {
                mAdapter.add(b);
            }
        }
    }

    public void getCerveses() {

        Activity activity = getActivity();

        if (mCerveses == null && activity != null) {
            // This is where we make our REST call to the service. We also pass in our ResultReceiver
            // defined in the RESTResponderFragment super class.

            // We will explicitly call our Service since we probably want to keep it as a private
            // component in our app. You could do this with Intent actions as well, but you have
            // to make sure you define your intent filters correctly in your manifest.
            Intent intent = new Intent(activity, RESTService.class);

            // IMPORTANT: canvieu l'URL depenent de si voleu treballar amb el servidor local o el OpenShift
            //intent.setData(Uri.parse("https://project2-pdsudg.rhcloud.com/rest/Usuaris"));
            intent.setData(Uri.parse("http://10.0.2.2:8080/rest/beers/"));

            // Here we are going to place our REST call parameters. Note that
            // we could have just used Uri.Builder and appendQueryParameter()
            // here, but I wanted to illustrate how to use the Bundle params.

            // Bundle params = new Bundle();
            intent.putExtra(RESTService.EXTRA_RESULT_RECEIVER, RESTService.GET);

            // Finally we put a ResultReceiver into the intent, that the RESTService
            // will use to send back the results
            intent.putExtra(RESTService.EXTRA_RESULT_RECEIVER, new ResultReceiver(new Handler()) {
                @Override
                protected void onReceiveResult(int resultCode, Bundle resultData) {
                    String xml = resultData.getString(RESTService.REST_RESULT);
                    onRESTResult(resultCode, xml);
                }});

            // Here we send our Intent to our RESTService.
            activity.startService(intent);
        }
    }

    public void onRESTResult(int code, String result) {
        // Here is where we handle our REST response. This is similar to the
        // LoaderCallbacks<D>.onLoadFinished() call from the previous tutorial.

        // Check to see if we got an HTTP 200 code and have some data.
        if (code == 200 && result != null) {
            try {
                mCerveses = getCervesesFromXML(result);
                showBeerList(mCerveses);
            } catch (Exception ex) {
                // There has been an error
                XMLError e = getErrorFromXML(result);
                mCallback.onErrorCerveses(e);
            }
        }
        else {
            Activity activity = getActivity();
            if (activity != null) {
                Toast.makeText(activity, "Failed to load Comments data. Check your internet settings.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private static BeerListREST getCervesesFromXML(String xml) {
        XStream xstream = new XStream(); // does not require XPP3 library starting with Java 6
        xstream.processAnnotations(BeerListREST.class);
        xstream.processAnnotations(BeerListREST.BeerREST.class);
        BeerListREST cervesesList = (BeerListREST) xstream.fromXML(xml);

        return cervesesList;
    }

    private static XMLError getErrorFromXML(String xml)
    {
        XStream xstream = new XStream(); // does not require XPP3 library starting with Java 6
        xstream.processAnnotations(XMLError.class);
        XMLError error = (XMLError) xstream.fromXML(xml);

        return error;
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        // Do something with the data
        Toast.makeText(getActivity(),
                "Click ListItem Number " + position, Toast.LENGTH_LONG)
                .show();
        Intent i = new Intent(getActivity(), PerfilCervesaActivity.class);
        //TextView tv = v.findViewById(R.layout.)
        i.putExtra("idCervesa",v.getId()); //TODO: falta modificar
        startActivity(i);
    }
}